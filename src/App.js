import React, {Component} from 'react';
import {BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import Home from './modules/home/home.component';
import Category from './modules/category/category.component';

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <h1 className="App-title">Welcome to React</h1>
                    </header>
                    <p className="App-intro">
                        To get started, edit <code>src/App.js</code> and save to reload.
                    </p>
                    <Link to="/doi-song">Đời sống</Link>
                    <Link to="/the-gioi">Thế giới</Link>
                    <Switch>
                        <Route exact={true} path="/" component={Home} />
                        <Route exact={true} path="/:categorySlug" component={Category} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
