import React, {Component} from 'react'

export default class Category extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        // làm sao lấy được cái params mới ra để xử lý trên nó?
        // thay đổi state chẳng hạn?
        this.state = {categorySlug:props.match.params.categorySlug};
    }

    render() {
        return (
            <div>
                <h1>Category Page</h1>
                <p>Category slug: {this.state.categorySlug}</p>
            </div>
        )
    }
}